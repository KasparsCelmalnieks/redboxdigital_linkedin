<?php
/**
 * @category    RedboxDigital
 * @package     RedboxDigital_Linkedin
 */

class RedboxDigital_Linkedin_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_REQUIRE_LINKEDIN = 'customer/create_account/require_linkedin';

    /**
     * @return int
     */
    public function isLinkedInRequired()
    {
        return Mage::getStoreConfig(self::XML_PATH_REQUIRE_LINKEDIN);
    }
}