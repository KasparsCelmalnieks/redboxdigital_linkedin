<?php
/**
 * @category    RedboxDigital
 * @package     RedboxDigital_Linkedin
 */

class RedboxDigital_Linkedin_Model_Observer
{
    public function saveLinkedInUrl(Varien_Event_Observer $observer)
    {
        $event = $observer->getEvent();
        $customer = $event->getCustomer();
        $linkedinUrl = Mage::app()->getRequest()->getPost('linkedin_profile');

        if (isset($linkedinUrl)) {
            $customer->setLinkedinProfile($linkedinUrl)->save();
        }
    }

    public function editLinkedInUrl(Varien_Event_Observer $observer)
    {
        $request = Mage::app()->getRequest();
        if ($request->getActionName() == 'editPost' && $request->getControllerName() == 'account') {
            $customer = Mage::getSingleton('customer/session')->getCustomer();
            $customer->setLinkedinProfile(Mage::app()->getRequest()->getPost('linkedin_profile'))->save();
        }
    }
}