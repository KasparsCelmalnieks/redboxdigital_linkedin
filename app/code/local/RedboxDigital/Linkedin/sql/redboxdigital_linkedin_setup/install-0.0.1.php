<?php
/**
 * @category    RedboxDigital
 * @package     RedboxDigital_Linkedin
 */

$installer = $this;
$installer->startSetup();

$setup = new Mage_Eav_Model_Entity_Setup('core_setup');

$entityTypeId     = $setup->getEntityTypeId('customer');
$attributeSetId   = $setup->getDefaultAttributeSetId($entityTypeId);
$attributeGroupId = $setup->getDefaultAttributeGroupId($entityTypeId, $attributeSetId);

$this->removeAttribute('customer', 'linkedin_profile');
$setup->addAttribute('customer', 'linkedin_profile', array(
    'type'              => 'varchar',
    'input'             => 'text',
    'label'             => 'LinkedIn Url',
    'global'            => true,
    'visible'           => true,
    'required'          => false,
    'user_defined'      => true,
    'visible_on_front'  => true
));

$setup->addAttributeToGroup(
    $entityTypeId,
    $attributeSetId,
    $attributeGroupId,
    'linkedin_profile',
    '999'
);

$oAttribute = Mage::getSingleton('eav/config')->getAttribute('customer', 'linkedin_profile');
$oAttribute->setData('used_in_forms', array('adminhtml_customer'));
$oAttribute->save();

$setup->endSetup();